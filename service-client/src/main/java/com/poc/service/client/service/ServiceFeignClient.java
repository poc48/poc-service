package com.poc.service.client.service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.poc.common.api.ResponseDto;
import com.poc.service.api.service.BusinessDto;
import com.poc.service.api.service.CategoryDto;
import com.poc.service.api.service.DoBusinessDto;
import com.poc.service.api.service.ServiceApi;
import feign.RequestLine;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public interface ServiceFeignClient extends ServiceApi {
    @Override
    @RequestLine("POST /businesses/do")
    @JsonIgnoreProperties(ignoreUnknown = true)
    ResponseDto doBusiness(DoBusinessDto dto);

    @Override
    @RequestLine("GET /businesses")
    List<BusinessDto> getAllBusinesses();

    @Override
    @RequestLine("GET /businesses/categories")
    List<CategoryDto> getAllCategories();
}
