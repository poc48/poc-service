package com.poc.service.client.service;

import com.poc.common.client.FeignClientBuilder;

public class ServiceFeignClientBuilder extends FeignClientBuilder<ServiceFeignClient> {
    public ServiceFeignClientBuilder(String url) {
        super(ServiceFeignClient.class, url);
    }
}
