package com.poc.service.client.service;

import com.poc.common.api.ResponseDto;
import com.poc.service.api.service.BusinessDto;
import com.poc.service.api.service.CategoryDto;
import com.poc.service.api.service.DoBusinessDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Disabled("Run when backed service is up")
class ServiceFeignClientImplTest {

    ServiceFeignClient client;

    @BeforeEach
    public void init() {
        client = new ServiceFeignClientBuilder("http://localhost:8090").build();
    }

    @Test
    void getAllCategories() {
        List<CategoryDto> actual = client.getAllCategories();

        assertFalse(actual.isEmpty());
    }

    @Test
    void createAndReceiveBusinessEntity() {
        var request = new DoBusinessDto();
        request.setTitle("title");
        request.setElementNumber(1);
        request.setCategory(1);

        ResponseDto saveResult = client.doBusiness(request);

        List<BusinessDto> businesses = client.getAllBusinesses();

        assertTrue(businesses.stream().anyMatch(item -> item.getId().toString().equals(saveResult.getResult())));
    }
}
