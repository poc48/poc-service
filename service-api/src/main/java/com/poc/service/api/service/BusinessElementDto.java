package com.poc.service.api.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BusinessElementDto {
    @JsonProperty("id")
    private UUID id;

    @JsonProperty("number")
    @Min(value = 1, message = "service.business_element.error.invalid_number")
    @NotBlank(message = "service.error.blank")
    @NotNull(message = "service.error.null")
    private Integer number;

    @JsonProperty("businessId")
    @NotNull(message = "service.error.null")
    @Min(value = 1, message = "service.error.business_not_found")
    private UUID businessId;
}
