package com.poc.service.api.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DoBusinessDto {
    @JsonProperty("title")
    @Size(min = 1, max = 255, message = "service.business.error.invalid_title")
    @NotBlank(message = "service.error.blank")
    @NotNull(message = "service.error.null")
    private String title;

    @JsonProperty("elementNumber")
    @Min(value = 1, message = "service.business.error.invalid_element_number")
    @NotNull(message = "service.error.null")
    private Integer elementNumber;

    @JsonProperty("category")
    @NotNull(message = "service.error.null")
    @Min(value = 1, message = "service.error.category_not_found")
    private Integer category;
}
