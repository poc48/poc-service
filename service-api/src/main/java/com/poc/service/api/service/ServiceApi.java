package com.poc.service.api.service;

import com.poc.common.api.ResponseDto;

import java.util.List;

public interface ServiceApi {
    ResponseDto doBusiness(DoBusinessDto dto);

    List<BusinessDto> getAllBusinesses();

    List<CategoryDto> getAllCategories();
}
