package com.poc.service.infrastructure.service.persistence.mapper;

import com.poc.service.domain.Business;
import com.poc.service.domain.BusinessElement;
import com.poc.service.domain.BusinessTitle;
import com.poc.service.infrastructure.service.persistence.BusinessElementEntity;
import com.poc.service.infrastructure.service.persistence.BusinessEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class BusinessEntityMapper {

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(
            target = "categoryId",
            expression = "java(model.getCategory())"
    )
    public abstract BusinessEntity toEntity(Business model);


    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    public abstract BusinessElementEntity toEntity(BusinessElement model);

    @Mapping(
            target = "category",
            expression = "java(entity.getCategoryId())"
    )
    public abstract Business toModel(BusinessEntity entity);

    @Mapping(target = "elements.updatedAt", ignore = true)
    public abstract List<BusinessEntity> toEntities(List<Business> models);

    public abstract List<Business> toModels(List<BusinessEntity> entities);

    String map(BusinessTitle value) {
        return value.getTitle();
    }

    BusinessTitle map(String value) {
        return new BusinessTitle(value);
    }
}
