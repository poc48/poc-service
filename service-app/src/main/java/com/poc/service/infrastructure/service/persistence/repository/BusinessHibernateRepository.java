package com.poc.service.infrastructure.service.persistence.repository;

import com.poc.service.infrastructure.service.persistence.BusinessEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface BusinessHibernateRepository extends CrudRepository<BusinessEntity, UUID> {
}
