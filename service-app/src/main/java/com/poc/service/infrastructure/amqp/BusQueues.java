package com.poc.service.infrastructure.amqp;

public class BusQueues {

    public static final String CATEGORY_CREATED = "core.category.created";
    public static final String CATEGORY_DELETED = "core.category.deleted";

    private BusQueues() {
    }
}
