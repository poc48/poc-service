package com.poc.service.infrastructure.amqp;

import com.poc.common.amqp.RabbitConfiguration;
import com.poc.common.api.amqp.BusExchanges;
import com.poc.common.api.amqp.BusRoutingKeys;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

@ComponentScan(basePackageClasses = RabbitConfiguration.class, useDefaultFilters = false, includeFilters = {
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = RabbitConfiguration.class)
})
@Configuration
public class AmqpConfiguration {

    @Bean
    public Declarables topicBindings() {
        var categoryCreatedQueue = new Queue(BusQueues.CATEGORY_CREATED, true);
        var categoryDeletedQueue = new Queue(BusQueues.CATEGORY_DELETED, true);

        var coreExchange = new TopicExchange(BusExchanges.CORE, true, false);

        return new Declarables(
                categoryCreatedQueue,
                categoryDeletedQueue,
                coreExchange,
                BindingBuilder.bind(categoryCreatedQueue).to(coreExchange).with(BusRoutingKeys.CATEGORY_CREATED),
                BindingBuilder.bind(categoryDeletedQueue).to(coreExchange).with(BusRoutingKeys.CATEGORY_DELETED)
        );
    }
}
