package com.poc.service.infrastructure.service.persistence;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity(name = "Business")
@Getter
@Setter
public class BusinessEntity {
    @Id
    @Column(name = "id", columnDefinition = "BINARY(16)", updatable = false, nullable = false)
    private UUID id;

    @OneToMany(mappedBy = "businessId", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<BusinessElementEntity> elements = new ArrayList<>();

    @NotBlank
    @Size(min = 1, max = 255, message = "service.business.error.invalid_title")
    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private Integer categoryId;

    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    private LocalDateTime createdAt;
}
