package com.poc.service.infrastructure.core.persistence.repository;

import com.poc.service.domain.core.Category;
import com.poc.service.domain.core.CategoryRepository;
import com.poc.service.infrastructure.core.persistence.CategoryEntity;
import com.poc.service.infrastructure.core.persistence.mapper.CategoryEntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {
    private final CategoryHibernateRepository categoryHibernateRepository;
    @Autowired
    private CategoryEntityMapper mapper;

    public CategoryRepositoryImpl(final CategoryHibernateRepository categoryHibernateRepository) {
        this.categoryHibernateRepository = categoryHibernateRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Category> findById(Integer id) {
        Optional<CategoryEntity> categoryHibernate = categoryHibernateRepository.findById(id);
        return categoryHibernate.map(entity -> mapper.toModel(entity));
    }

    @Override
    @Transactional(readOnly = true)
    public List<Category> findAll() {
        Iterable<CategoryEntity> categoryEntities = categoryHibernateRepository.findAll();
        return mapper.toModels((List<CategoryEntity>) categoryEntities);
    }

    @Override
    @Transactional
    public void save(Category model) {
        CategoryEntity categoryEntity = mapper.toEntity(model);
        categoryHibernateRepository.save(categoryEntity);
    }

    @Override
    @Transactional
    public void delete(Category model) {
        CategoryEntity categoryEntity = mapper.toEntity(model);
        categoryHibernateRepository.delete(categoryEntity);
    }
}
