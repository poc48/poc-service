package com.poc.service.infrastructure.core.persistence.mapper;

import com.poc.service.domain.core.Category;
import com.poc.service.infrastructure.core.persistence.CategoryEntity;
import com.poc.service.infrastructure.service.persistence.mapper.BusinessEntityMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = BusinessEntityMapper.class)
public abstract class CategoryEntityMapper {
    public abstract CategoryEntity toEntity(Category model);

    public abstract Category toModel(CategoryEntity entity);

    public abstract List<Category> toModels(List<CategoryEntity> entities);

    Category map(Integer value) {
        return new Category(value);
    }
}
