package com.poc.service.infrastructure.service.persistence.repository;

import com.poc.service.domain.Business;
import com.poc.service.domain.BusinessRepository;
import com.poc.service.infrastructure.service.persistence.BusinessEntity;
import com.poc.service.infrastructure.service.persistence.mapper.BusinessEntityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class BusinessRepositoryImpl implements BusinessRepository {
    private final BusinessHibernateRepository businessHibernateRepository;
    @Autowired
    private BusinessEntityMapper mapper;

    public BusinessRepositoryImpl(final BusinessHibernateRepository businessHibernateRepository) {
        this.businessHibernateRepository = businessHibernateRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Business> findAll() {
        List<BusinessEntity> businessEntities = (List<BusinessEntity>) businessHibernateRepository.findAll();
        return mapper.toModels(businessEntities);
    }
}
