package com.poc.service.infrastructure.service.persistence.mapper;

import com.poc.service.domain.BusinessElement;
import com.poc.service.infrastructure.service.persistence.BusinessElementEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class BusinessElementEntityMapper {

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    public abstract BusinessElementEntity toEntity(BusinessElement model);

    public abstract BusinessElement toModel(BusinessElementEntity entity);

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    public abstract List<BusinessElementEntity> toEntities(List<BusinessElement> models);

    public abstract List<BusinessElement> toModels(List<BusinessElementEntity> entities);
}
