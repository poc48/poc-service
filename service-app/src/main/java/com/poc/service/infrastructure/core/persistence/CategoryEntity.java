package com.poc.service.infrastructure.core.persistence;

import com.poc.service.infrastructure.service.persistence.BusinessEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Category")
@Getter
@Setter
public class CategoryEntity {
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;

    @OneToMany(mappedBy = "categoryId", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<BusinessEntity> businesses = new HashSet<>();
}
