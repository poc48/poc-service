package com.poc.service.infrastructure.core.persistence.repository;

import com.poc.service.infrastructure.core.persistence.CategoryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryHibernateRepository extends CrudRepository<CategoryEntity, Integer> {
}
