package com.poc.service.application.listener.core;

import com.poc.common.api.amqp.BusMessage;
import com.poc.service.application.service.category.DeleteCategory;
import com.poc.service.infrastructure.amqp.BusQueues;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryDeletedListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryDeletedListener.class);
    private final DeleteCategory deleteCategory;

    @RabbitListener(queues = BusQueues.CATEGORY_DELETED)
    public void onCategoryMessage(BusMessage message) {
        LOGGER.info("Delete category event {}", message);

        deleteCategory.execute(Integer.valueOf(message.getPayload()));
    }
}
