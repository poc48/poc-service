package com.poc.service.application.service.category;

import com.poc.service.domain.core.Category;
import com.poc.service.domain.core.CategoryRepository;
import org.springframework.stereotype.Service;

@Service
public class AddCategory {
    private final CategoryRepository categoryRepository;

    public AddCategory(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public void execute(Integer id) {
        Category category = new Category(id);
        categoryRepository.save(category);
    }
}
