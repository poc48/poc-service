package com.poc.service.application.mapper;

import com.poc.service.api.service.BusinessElementDto;
import com.poc.service.domain.BusinessElement;
import com.poc.service.domain.BusinessElementNumber;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring")
public interface BusinessElementMapper {
    BusinessElementDto toDto(BusinessElement element);

    BusinessElement fromDto(BusinessElementDto dto);

    List<BusinessElementDto> toDtos(List<BusinessElement> models);

    List<BusinessElement> fromDtos(List<BusinessElementDto> dtos);

    default String mapUuidToStr(UUID value) {
        return value.toString();
    }

    default UUID mapStrToUuid(String value) {
        return UUID.fromString(value);
    }

    default Integer map(BusinessElementNumber value) {
        return value.getNumber();
    }

    default BusinessElementNumber map(Integer value) {
        return new BusinessElementNumber(value);
    }
}
