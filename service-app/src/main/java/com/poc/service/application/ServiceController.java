package com.poc.service.application;

import com.poc.common.api.ResponseDto;
import com.poc.service.api.service.BusinessDto;
import com.poc.service.api.service.CategoryDto;
import com.poc.service.api.service.DoBusinessDto;
import com.poc.service.api.service.ServiceApi;
import com.poc.service.application.mapper.BusinessMapper;
import com.poc.service.domain.BusinessRepository;
import com.poc.service.domain.core.CategoryRepository;
import com.poc.service.domain.usecase.DoBusiness;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("businesses")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ServiceController implements ServiceApi {
    private final BusinessMapper businessMapper;
    private final DoBusiness doBusinessService;

    private final CategoryRepository categoryRepository;
    private final BusinessRepository businessRepository;

    @Override
    @Transactional
    @PostMapping(
            path = "do",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseDto doBusiness(@Valid @RequestBody DoBusinessDto dto) {
        var responseDto = new ResponseDto();
        var request = new DoBusiness.DoBusinessRequest(dto.getTitle(), dto.getElementNumber(), dto.getCategory());
        responseDto.setResult(doBusinessService.doBusiness(request).getId().toString());
        return responseDto;
    }

    @Override
    @GetMapping(path = "categories", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CategoryDto> getAllCategories() {
        return categoryRepository.findAll().stream().map(v -> {
            CategoryDto categoryDto = new CategoryDto();
            categoryDto.setId(v.getId());
            return categoryDto;
        }).collect(Collectors.toList());
    }

    @Override
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BusinessDto> getAllBusinesses() {
        return businessMapper.toDtos(new ArrayList<>(businessRepository.findAll()));
    }
}
