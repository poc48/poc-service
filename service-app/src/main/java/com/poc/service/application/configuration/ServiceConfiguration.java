package com.poc.service.application.configuration;

import com.poc.service.application.service.category.AddCategory;
import com.poc.service.application.service.category.DeleteCategory;
import com.poc.service.domain.core.CategoryRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {
    @Bean
    AddCategory createCategoryService(CategoryRepository categoryRepository) {
        return new AddCategory(categoryRepository);
    }

    @Bean
    DeleteCategory deleteCategoryService(CategoryRepository categoryRepository) {
        return new DeleteCategory(categoryRepository);
    }
}
