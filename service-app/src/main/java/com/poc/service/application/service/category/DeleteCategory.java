package com.poc.service.application.service.category;

import com.poc.service.domain.core.CategoryRepository;
import com.poc.service.domain.exception.DomainException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DeleteCategory {
    private final CategoryRepository categoryRepository;

    public DeleteCategory(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public void execute(Integer id) {
        var categoryOpt = categoryRepository.findById(id);
        categoryOpt.map(c -> {
            c.setBusinesses(List.of());
            categoryRepository.delete(c);
            return c;
        }).orElseThrow(() -> new DomainException(
                "category",
                "service.category.error.not_found",
                "Category not found."
        ));
    }
}
