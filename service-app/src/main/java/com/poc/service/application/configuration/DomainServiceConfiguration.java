package com.poc.service.application.configuration;

import com.poc.service.domain.core.CategoryRepository;
import com.poc.service.domain.usecase.DoBusiness;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainServiceConfiguration {
    @Bean
    DoBusiness doBusinessService(CategoryRepository categoryRepository) {
        return new DoBusiness(categoryRepository);
    }
}
