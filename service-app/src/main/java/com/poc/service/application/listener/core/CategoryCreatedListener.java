package com.poc.service.application.listener.core;

import com.poc.common.api.amqp.BusMessage;
import com.poc.service.application.service.category.AddCategory;
import com.poc.service.infrastructure.amqp.BusQueues;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CategoryCreatedListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryCreatedListener.class);
    private final AddCategory addCategory;

    @RabbitListener(queues = BusQueues.CATEGORY_CREATED)
    public void onCategoryMessage(BusMessage message) {
        LOGGER.info("Create category event {}", message);

        addCategory.execute(Integer.valueOf(message.getPayload()));
    }
}
