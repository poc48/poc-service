package com.poc.service.application.common;

import com.poc.common.api.ServiceError;
import com.poc.common.api.ServiceValidationException;
import com.poc.common.service.error.GlobalExceptionHandler;
import com.poc.service.domain.exception.DomainException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionController extends GlobalExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionController.class);

    @ExceptionHandler(EmptyResultDataAccessException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ServiceError handleNoDataFound(EmptyResultDataAccessException ex) {
        LOGGER.error("No entity found", ex);
        ServiceError serviceError = new ServiceError();
        serviceError.setStatus(HttpStatus.NOT_FOUND.toString());
        serviceError.setLabel("service.error.entity_not_found");
        serviceError.setMessage(ex.getMessage());
        serviceError.setStatusDescription("Not found entity to process");
        return serviceError;
    }

    @ExceptionHandler(DomainException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ServiceError handleDomainException(DomainException ex) {
        LOGGER.error("Domain exception occurred", ex);

        ServiceError serviceError = new ServiceError();
        serviceError.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
        serviceError.setLabel(ex.getLabel());
        serviceError.setMessage(ex.getDescription());
        serviceError.setStatusDescription("Domain error occurred.");
        return serviceError;
    }

    /**
     * Handle MethodArgumentNotValidException. Triggered when an object fails @Valid validation.
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ServiceValidationException handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        LOGGER.error("Validation exception", ex);

        ServiceValidationException validationException = new ServiceValidationException(
                "service.error.validation",
                "One or multiple validation exceptions occurred."
        );
        ex.getBindingResult().getFieldErrors().forEach(fieldError -> validationException.addValidationError(
                new ServiceError(
                        fieldError.getField(),
                        fieldError.getDefaultMessage()
                )
        ));
        return validationException;
    }
}
