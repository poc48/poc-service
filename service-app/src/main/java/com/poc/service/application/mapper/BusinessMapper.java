package com.poc.service.application.mapper;

import com.poc.service.api.service.BusinessDto;
import com.poc.service.api.service.BusinessElementDto;
import com.poc.service.domain.Business;
import com.poc.service.domain.BusinessElement;
import com.poc.service.domain.BusinessTitle;
import com.poc.service.domain.core.Category;
import org.mapstruct.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper(componentModel = "spring")
public abstract class BusinessMapper {

    public abstract BusinessDto toDto(Business document);

    public abstract Business fromDto(BusinessDto dto);

    public abstract List<BusinessDto> toDtos(List<Business> models);

    public abstract List<Business> fromDtos(List<BusinessDto> dtos);

    String mapUuidToStr(UUID value) {
        return value.toString();
    }

    UUID mapStrToUuid(String value) {
        return UUID.fromString(value);
    }

    Category map(Integer value) {
        return new Category(value);
    }

    String map(BusinessTitle value) {
        return value.getTitle();
    }

    BusinessTitle map(String value) {
        return new BusinessTitle(value);
    }

    Category map(BusinessDto value) {
        return new Category(value.getCategory());
    }

    BusinessElementDto map(BusinessElement value) {
        var dto = new BusinessElementDto();
        dto.setId(value.getId());
        dto.setBusinessId(value.getBusinessId());
        dto.setNumber(value.getNumber());
        return dto;
    }
}
