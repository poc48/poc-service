package com.poc.service.domain;

import com.poc.service.domain.exception.DomainException;
import lombok.Value;

@Value
public class BusinessElementNumber {
    Integer number;

    public BusinessElementNumber(Integer number) {
        validate(number);
        this.number = number;
    }

    private void validate(Integer number) {
        if (number == null || number < 1) {
            throw new DomainException(
                    "number",
                    "service.business_element.error.invalid_number",
                    "Number has invalid value."
            );
        }
    }
}
