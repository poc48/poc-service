package com.poc.service.domain;

import com.poc.service.domain.exception.DomainException;
import lombok.Value;

@Value
public class BusinessTitle {
    String title;

    public BusinessTitle(String title) {
        validate(title);
        this.title = title;
    }

    private void validate(String text) {
        if (text == null || text.trim().length() < 2 || text.length() > 255) {
            throw new DomainException(
                    "title",
                    "service.business.error.invalid_title",
                    "Title has invalid length."
            );
        }
    }
}
