package com.poc.service.domain.exception;

public class DomainException extends RuntimeException {
    private final String label;
    private final String field;
    private final String description;

    public DomainException(String field, String label, String description) {
        this.label = label;
        this.field = field;
        this.description = description;
    }

    public String getLabel() {
        return label;
    }

    public String getField() {
        return field;
    }

    public String getDescription() {
        return description;
    }
}
