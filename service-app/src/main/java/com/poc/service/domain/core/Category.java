package com.poc.service.domain.core;

import com.poc.service.domain.Business;
import com.poc.service.domain.BusinessElementNumber;
import com.poc.service.domain.BusinessTitle;
import com.poc.service.domain.exception.DomainException;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@EqualsAndHashCode
public class Category {
    private final Integer id;
    private List<Business> businesses = new ArrayList<>();

    public Category(@NonNull Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setBusinesses(@NotNull List<Business> businesses) {
        List<Business> businessesBelongToAnotherCategories = businesses.stream()
                .filter(v -> !v.getCategory().equals(this.id))
                .collect(Collectors.toList());
        if (!businessesBelongToAnotherCategories.isEmpty()) {
            throw new DomainException(
                    "businesses",
                    "service.category.error.invalid_businesses",
                    "Passed data belongs to another category."
            );
        }
        this.businesses = businesses;
    }

    public List<Business> getBusinesses() {
        return Collections.unmodifiableList(businesses);
    }

    public Business doBusiness(@NonNull String title, Integer elementNumber) {
        var business = new Business(
                UUID.randomUUID(),
                new BusinessTitle(title),
                new BusinessElementNumber(elementNumber),
                this
        );
        this.businesses.add(business);
        return business;
    }
}
