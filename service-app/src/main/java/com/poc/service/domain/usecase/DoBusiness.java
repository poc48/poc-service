package com.poc.service.domain.usecase;

import com.poc.service.domain.Business;
import com.poc.service.domain.core.Category;
import com.poc.service.domain.core.CategoryRepository;
import com.poc.service.domain.exception.DomainException;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

public class DoBusiness {
    private final CategoryRepository categoryRepository;

    public DoBusiness(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Business doBusiness(DoBusinessRequest request) {
        Category category = receiveCategory(request.getCategory());
        Business result = category.doBusiness(request.getTitle(), request.getElementNumber());
        categoryRepository.save(category);
        return result;
    }

    private Category receiveCategory(Integer id) {
        Optional<Category> categoryOptional = categoryRepository.findById(id);
        if (categoryOptional.isEmpty()) {
            throw new DomainException(
                    "id",
                    "service.error.category_not_found",
                    "Unable to find Category by ID."
            );
        }
        return categoryOptional.get();
    }

    @Getter
    @RequiredArgsConstructor
    public static final class DoBusinessRequest {
        private final String title;
        private final Integer elementNumber;
        private final Integer category;
    }
}
