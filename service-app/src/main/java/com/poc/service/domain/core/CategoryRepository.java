package com.poc.service.domain.core;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository {
    Optional<Category> findById(Integer id);

    List<Category> findAll();

    void save(Category model);

    void delete(Category model);
}
