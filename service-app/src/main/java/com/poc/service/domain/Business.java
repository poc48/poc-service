package com.poc.service.domain;

import com.poc.service.domain.annotation.Default;
import com.poc.service.domain.core.Category;
import com.poc.service.domain.exception.DomainException;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Getter
@EqualsAndHashCode
public class Business {
    private final UUID id;
    private final BusinessTitle title;
    private final Integer category;
    private final List<BusinessElement> elements = new ArrayList<>();

    public Business(
            @NonNull UUID id,
            @NonNull BusinessTitle title,
            @NonNull BusinessElementNumber elementNumber,
            @NonNull Category category
    ) {
        this.id = id;
        this.title = title;
        this.category = category.getId();
        this.elements.add(new BusinessElement(this, elementNumber));
    }

    @Default
    public Business(UUID id, BusinessTitle title, Integer category, Set<BusinessElement> elements) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.elements.addAll(elements);
    }

    public UUID addElement(BusinessElementNumber number) {
        boolean isFound = this.elements.stream().anyMatch(elm -> elm.getNumber().equals(number.getNumber()));
        if (isFound) {
            throw new DomainException(
                    "element",
                    "service.business.error.duplicated_element_number",
                    "Business should have duplicated elements."
            );
        }
        var newElement = new BusinessElement(this, number);
        this.elements.add(newElement);
        return newElement.getId();
    }
}
