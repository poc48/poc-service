package com.poc.service.domain;

import com.poc.service.domain.annotation.Default;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.UUID;

@Getter
@EqualsAndHashCode
public final class BusinessElement {
    private final UUID id;
    private final UUID businessId;
    private final Integer number;

    public BusinessElement(Business businessId, BusinessElementNumber number) {
        this.id = UUID.randomUUID();
        this.number = number.getNumber();
        this.businessId = businessId.getId();
    }

    @Default
    public BusinessElement(UUID id, UUID businessId, Integer number) {
        this.id = id;
        this.businessId = businessId;
        this.number = number;
    }
}
