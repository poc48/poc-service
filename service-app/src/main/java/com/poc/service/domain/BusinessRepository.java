package com.poc.service.domain;

import java.util.List;

public interface BusinessRepository {
    List<Business> findAll();
}
