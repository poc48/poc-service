package com.poc.service.application.listener.core;

import com.poc.common.api.amqp.BusExchanges;
import com.poc.common.api.amqp.BusMessage;
import com.poc.service.domain.core.Category;
import com.poc.service.domain.core.CategoryRepository;
import com.poc.service.infrastructure.amqp.BusQueues;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.system.CapturedOutput;
import org.springframework.boot.test.system.OutputCaptureExtension;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.sql.Timestamp;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(initializers = RabbitMqListenerIntegrationTest.Initializer.class)
@Testcontainers
@ExtendWith(OutputCaptureExtension.class)
class RabbitMqListenerIntegrationTest {
    @Container
    private static final RabbitMQContainer rabbit = new RabbitMQContainer("rabbitmq:3.9.5-alpine")
            .withExposedPorts(5672, 15672);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private CategoryRepository repository;

    @Test
    void testCreateCategory(CapturedOutput output) {
        var id = 1101;

        var message = new BusMessage();
        message.setMessageId(UUID.randomUUID().toString());
        message.setTimestamp(new Timestamp(System.currentTimeMillis()).toString());
        message.setPayload(String.valueOf(id));

        this.rabbitTemplate.convertAndSend(BusExchanges.CORE,
                BusQueues.CATEGORY_CREATED,
                message
        );

        await().atMost(5, TimeUnit.SECONDS)
                .until(isMessageConsumed(output, "Create category event"));

        var actual = repository.findById(id);
        assertTrue(actual.isPresent());
    }

    @Test
    void testDeleteCategory(CapturedOutput output) {
        var id = 1102;

        Category model = new Category(id);
        repository.save(model);

        var message = new BusMessage();
        message.setMessageId(UUID.randomUUID().toString());
        message.setTimestamp(new Timestamp(System.currentTimeMillis()).toString());
        message.setPayload(String.valueOf(id));

        this.rabbitTemplate.convertAndSend(BusExchanges.CORE,
                BusQueues.CATEGORY_DELETED,
                message
        );

        await().atMost(5, TimeUnit.SECONDS)
                .until(isMessageConsumed(output, "Delete category event"));

        var actual = repository.findById(id);
        assertFalse(actual.isPresent());
    }

    private Callable<Boolean> isMessageConsumed(CapturedOutput output, String message) {
        return () -> output.toString().contains(message);
    }

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            val values = TestPropertyValues.of(
                    "spring.rabbitmq.host=" + rabbit.getContainerIpAddress(),
                    "spring.rabbitmq.port=" + rabbit.getMappedPort(5672)
            );
            values.applyTo(configurableApplicationContext);
        }
    }
}
