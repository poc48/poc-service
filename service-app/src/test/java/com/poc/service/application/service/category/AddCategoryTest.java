package com.poc.service.application.service.category;

import com.poc.service.domain.core.Category;
import com.poc.service.domain.core.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class AddCategoryTest {
    @Mock
    private CategoryRepository categoryRepository;
    @InjectMocks
    private AddCategory service;

    @Test
    void execute() {
        service.execute(1);

        verify(categoryRepository, times(1)).save(any(Category.class));
    }
}