package com.poc.service.application.service.category;

import com.poc.service.domain.core.Category;
import com.poc.service.domain.core.CategoryRepository;
import com.poc.service.domain.exception.DomainException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DeleteCategoryTest {
    @Mock
    private CategoryRepository categoryRepository;
    @InjectMocks
    private DeleteCategory service;

    @Test
    void elementAndBusinessAreRemoved() {
        var category = new Category(1);
        category.doBusiness("business title", 1);

        ArgumentCaptor<Category> categoryCaptor = ArgumentCaptor.forClass(Category.class);

        when(categoryRepository.findById(category.getId())).thenReturn(Optional.of(category));

        service.execute(category.getId());

        verify(categoryRepository, times(1)).delete(categoryCaptor.capture());

        var actual = categoryCaptor.getValue();

        assertEquals(0, actual.getBusinesses().size());
    }

    @Test
    void deleteNonExistentCategory() {
        when(categoryRepository.findById(anyInt())).thenReturn(Optional.empty());
        assertThrows(DomainException.class, () -> service.execute(0));
    }
}