package com.poc.service.application;

import com.poc.common.api.ResponseDto;
import com.poc.service.api.service.BusinessDto;
import com.poc.service.api.service.CategoryDto;
import com.poc.service.api.service.DoBusinessDto;
import com.poc.service.application.mapper.BusinessMapper;
import com.poc.service.domain.Business;
import com.poc.service.domain.BusinessElementNumber;
import com.poc.service.domain.BusinessRepository;
import com.poc.service.domain.BusinessTitle;
import com.poc.service.domain.core.Category;
import com.poc.service.domain.core.CategoryRepository;
import com.poc.service.domain.usecase.DoBusiness;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class ServiceControllerTest {
    BusinessMapper businessMapper = Mappers.getMapper(BusinessMapper.class);
    @Mock
    DoBusiness doBusinessService;
    @Mock
    CategoryRepository categoryRepository;
    @Mock
    BusinessRepository businessRepository;

    @InjectMocks
    private ServiceController controller;

    @BeforeEach
    void setUp() {
        controller = new ServiceController(businessMapper, doBusinessService, categoryRepository, businessRepository);
    }

    @Test
    void getAllCategories() {
        Mockito.when(categoryRepository.findAll()).thenReturn(List.of(new Category(1)));

        List<CategoryDto> actual = controller.getAllCategories();

        assertEquals(1, actual.size());
    }

    @Test
    void getAllBusinesses() {
        Business expected = new Business(
                UUID.randomUUID(),
                new BusinessTitle("title"),
                new BusinessElementNumber(1),
                new Category(1)
        );

        Mockito.when(businessRepository.findAll()).thenReturn(List.of(expected));

        List<BusinessDto> actual = controller.getAllBusinesses();

        assertEquals(expected.getId(), actual.get(0).getId());
    }

    @Test
    void doBusiness() {
        Business expected = new Business(
                UUID.randomUUID(),
                new BusinessTitle("title"),
                new BusinessElementNumber(1),
                new Category(1)
        );

        Mockito.when(doBusinessService.doBusiness(any())).thenReturn(expected);

        var request = new DoBusinessDto();
        request.setTitle("title");
        request.setElementNumber(1);
        request.setCategory(1);

        ResponseDto actual = controller.doBusiness(request);

        assertEquals(expected.getId().toString(), actual.getResult());
    }
}