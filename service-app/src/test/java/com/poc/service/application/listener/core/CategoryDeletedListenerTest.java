package com.poc.service.application.listener.core;

import com.poc.common.api.amqp.BusMessage;
import com.poc.service.application.service.category.DeleteCategory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.UUID;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class CategoryDeletedListenerTest {
    @Mock
    private DeleteCategory deleteCategory;
    @InjectMocks
    private CategoryDeletedListener listener;

    @Test
    void onCategoryMessage() {
        BusMessage message = new BusMessage();
        message.setMessageId(UUID.randomUUID().toString());
        message.setTimestamp(String.valueOf(new Timestamp(System.currentTimeMillis())));
        message.setPayload("1");

        listener.onCategoryMessage(message);

        verify(deleteCategory, times(1)).execute(1);
    }
}