package com.poc.service.application.mapper;

import com.poc.service.api.service.BusinessDto;
import com.poc.service.api.service.BusinessElementDto;
import com.poc.service.domain.Business;
import com.poc.service.domain.BusinessElementNumber;
import com.poc.service.domain.BusinessTitle;
import com.poc.service.domain.core.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BusinessMapperTest {

    private BusinessMapper mapper;

    @BeforeEach
    public void init() {
        mapper = Mappers.getMapper(BusinessMapper.class);
    }

    @Test
    void toDto() {
        var expected = new Business(
                UUID.randomUUID(),
                new BusinessTitle("title"),
                new BusinessElementNumber(1),
                new Category(1)
        );

        var actual = mapper.toDto(expected);
        var actualList = mapper.toDtos(List.of(expected));

        assertEquals(1, actualList.size());
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getTitle().getTitle(), actual.getTitle());
        assertEquals(expected.getCategory(), actual.getCategory());
        assertEquals(expected.getElements().get(0).getId(), actual.getElements().get(0).getId());
    }

    @Test
    void fromDto() {
        var element = new BusinessElementDto();
        element.setNumber(1);
        element.setBusinessId(UUID.randomUUID());
        element.setId(UUID.randomUUID());

        var expected = new BusinessDto();
        expected.setCategory(1);
        expected.setElements(List.of(element));
        expected.setId(UUID.randomUUID());
        expected.setTitle("Title");

        var actual = mapper.fromDto(expected);
        var actualList = mapper.fromDtos(List.of(expected));

        assertEquals(1, actualList.size());
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getTitle(), actual.getTitle().getTitle());
        assertEquals(expected.getCategory(), actual.getCategory());
        assertEquals(expected.getElements().get(0).getId(), actual.getElements().get(0).getId());
        assertEquals(expected.getElements().get(0).getNumber(), actual.getElements().get(0).getNumber());
        assertEquals(expected.getElements().get(0).getBusinessId(), actual.getElements().get(0).getBusinessId());
    }
}