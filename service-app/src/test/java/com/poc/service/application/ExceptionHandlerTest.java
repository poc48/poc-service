package com.poc.service.application;

import com.poc.common.api.ServiceValidationException;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ExceptionHandlerTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void getSaveIncomingDataValidationErrors() throws JSONException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        JSONObject requestJson = new JSONObject();
        requestJson.put("title", "");
        requestJson.put("elementNumber", 1);
        requestJson.put("category", 1);

        HttpEntity<String> request = new HttpEntity<>(requestJson.toString(), headers);

        ResponseEntity<ServiceValidationException> actual = testRestTemplate.exchange(
                "/businesses/do",
                HttpMethod.POST,
                request,
                ServiceValidationException.class
        );

        assertEquals(HttpStatus.BAD_REQUEST, actual.getStatusCode());
        assertEquals(2, actual.getBody().getValidationErrors().size());
    }
}