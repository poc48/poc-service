package com.poc.service.domain;

import com.poc.service.domain.core.Category;
import com.poc.service.domain.exception.DomainException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class BusinessTest {

    @Test
    void addElement() {
        var business = new Business(
                UUID.randomUUID(),
                new BusinessTitle("title"),
                new BusinessElementNumber(1),
                new Category(1)
        );

        business.addElement(new BusinessElementNumber(2));

        assertEquals(2, business.getElements().size());
    }

    @Test
    void addDuplicatedElement() {
        var business = new Business(
                UUID.randomUUID(),
                new BusinessTitle("title"),
                new BusinessElementNumber(1),
                new Category(1)
        );

        assertThrows(DomainException.class, () -> {
            business.addElement(new BusinessElementNumber(1));
        });
    }
}
