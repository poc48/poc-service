package com.poc.service.domain.usecase;

import com.poc.service.domain.core.Category;
import com.poc.service.domain.core.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class DoBusinessTest {
    @Mock
    CategoryRepository repository;

    @InjectMocks
    DoBusiness service;

    @Test
    void newBusinessInstanceIsCreatedForCategory() {
        var category = new Category(1);

        Mockito.when(repository.findById(category.getId())).thenReturn(Optional.of(category));

        var request = new DoBusiness.DoBusinessRequest("title", 1, category.getId());
        service.doBusiness(request);

        assertEquals(1, category.getBusinesses().size());
    }
}