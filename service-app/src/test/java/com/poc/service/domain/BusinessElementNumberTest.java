package com.poc.service.domain;

import com.poc.service.domain.exception.DomainException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class BusinessElementNumberTest {

    @Test
    void numberValidation() {
        assertThrows(DomainException.class, () -> new BusinessElementNumber(null));
    }
}
