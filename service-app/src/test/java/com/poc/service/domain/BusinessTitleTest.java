package com.poc.service.domain;

import com.poc.service.domain.exception.DomainException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class BusinessTitleTest {

    @Test
    void titleValidation() {
        assertThrows(DomainException.class, () -> {
            new BusinessTitle(null);
        });
    }
}
