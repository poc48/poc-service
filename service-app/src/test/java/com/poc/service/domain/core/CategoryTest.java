package com.poc.service.domain.core;

import com.poc.service.domain.Business;
import com.poc.service.domain.BusinessElementNumber;
import com.poc.service.domain.BusinessTitle;
import com.poc.service.domain.exception.DomainException;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CategoryTest {
    @Test
    void addBusiness() {
        Category category = new Category(1);
        category.doBusiness("title", 1);

        assertEquals(1, category.getBusinesses().size());
    }

    @Test
    void setBusinessesOfAnotherCategory() {
        var category = new Category(1);

        var businessDifferentCategory = new Business(
                UUID.randomUUID(),
                new BusinessTitle("title"),
                new BusinessElementNumber(1),
                new Category(2)
        );

        assertThrows(DomainException.class, () -> {
            category.setBusinesses(List.of(businessDifferentCategory));
        });
    }

    @Test()
    void immutableListOfBusinesses() {
        Category category = new Category(1);
        category.doBusiness("title", 1);

        Business business = new Business(
                UUID.randomUUID(),
                new BusinessTitle("title"),
                new BusinessElementNumber(1),
                new Category(1));

        List<Business> actual = category.getBusinesses();

        assertThrows(
                UnsupportedOperationException.class,
                () -> actual.add(business),
                "Expected immutable set."
        );
    }

    @Test
    void testEqualsSymmetric() {
        var x = new Category(1);
        var y = new Category(1);
        assertTrue(x.equals(y) && y.equals(x));
        assertTrue(x.hashCode() == y.hashCode());
    }
}
