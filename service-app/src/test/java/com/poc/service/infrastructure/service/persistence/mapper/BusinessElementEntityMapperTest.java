package com.poc.service.infrastructure.service.persistence.mapper;

import com.poc.service.domain.Business;
import com.poc.service.domain.BusinessElement;
import com.poc.service.domain.BusinessElementNumber;
import com.poc.service.domain.BusinessTitle;
import com.poc.service.domain.core.Category;
import com.poc.service.infrastructure.service.persistence.BusinessElementEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BusinessElementEntityMapperTest {
    private BusinessElementEntityMapper mapper;

    @BeforeEach
    public void init() {
        mapper = Mappers.getMapper(BusinessElementEntityMapper.class);
    }

    @Test
    void toEntity() {
        var business = new Business(
                UUID.randomUUID(),
                new BusinessTitle("title"),
                new BusinessElementNumber(1),
                new Category(1)
        );
        var model = new BusinessElement(business, new BusinessElementNumber(1));

        var actual = mapper.toEntity(model);
        var actualList = mapper.toEntities(List.of(model));

        assertEquals(1, actualList.size());
        assertEquals(model.getId(), actual.getId());
        assertEquals(model.getNumber(), actual.getNumber());
        assertEquals(model.getBusinessId(), actual.getBusinessId());
    }

    @Test
    void toModel() {
        var entity = new BusinessElementEntity();
        entity.setId(UUID.randomUUID());
        entity.setNumber(1);
        entity.setBusinessId(UUID.randomUUID());

        var actual = mapper.toModel(entity);
        var actualList = mapper.toModels(List.of(entity));

        assertEquals(1, actualList.size());
        assertEquals(entity.getId(), actual.getId());
        assertEquals(entity.getNumber(), actual.getNumber());
        assertEquals(entity.getBusinessId(), actual.getBusinessId());
    }
}