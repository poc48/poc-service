package com.poc.service.infrastructure.core.persistence.repository;

import com.poc.service.domain.core.Category;
import com.poc.service.domain.core.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CategoryRepositoryImplTest {
    @Autowired
    private CategoryRepository repository;

    @Test
    @Transactional
    void findById() {
        Category category = new Category(1);
        repository.save(category);

        Optional<Category> actual = repository.findById(category.getId());

        assertTrue(actual.isPresent());
        assertEquals(category, actual.get());
    }

    @Test
    @Transactional
    void findAll() {
        Category category1 = new Category(1);
        Category category2 = new Category(2);
        repository.save(category1);
        repository.save(category2);

        List<Category> actual = repository.findAll();

        assertEquals(2, actual.size());
    }

    @Test
    @Transactional
    void equalOfAggregateRoot() {
        var category = new Category(1);
        category.doBusiness("Title", 1);
        repository.save(category);

        Optional<Category> actual = repository.findById(category.getId());

        assertTrue(actual.isPresent());
        assertEquals(category, actual.get());
    }

    @Test
    @Transactional
    void delete() {
        var category = new Category(1);
        repository.save(category);

        repository.delete(category);
        var actual = repository.findById(1);

        assertTrue(actual.isEmpty());
    }
}
