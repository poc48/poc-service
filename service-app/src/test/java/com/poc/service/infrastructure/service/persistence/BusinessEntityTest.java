package com.poc.service.infrastructure.service.persistence;

import com.poc.service.domain.core.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:businessFixture.sql")
@Transactional
class BusinessEntityTest {

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    void elementIsMappedFromCategory() {
        var category = categoryRepository.findById(1).orElse(null);
        assertNotNull(category);

        var business = category.getBusinesses().iterator().next();
        assertNotNull(business);

        var element = business.getElements().iterator().next();

        assertEquals(1, element.getNumber());
    }

    @Test
    void testCreatedAtIsSet() {
        var category = categoryRepository.findById(1).orElse(null);

        var business = category.getBusinesses().iterator().next();
        assertNotNull(business);

        var element = business.getElements().iterator().next();

        assertEquals(1, element.getNumber());
    }
}