package com.poc.service.infrastructure.service.persistence.mapper;

import com.poc.service.domain.Business;
import com.poc.service.domain.BusinessElementNumber;
import com.poc.service.domain.BusinessTitle;
import com.poc.service.domain.core.Category;
import com.poc.service.infrastructure.service.persistence.BusinessEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BusinessEntityMapperTest {
    private BusinessEntityMapper mapper;

    @BeforeEach
    public void init() {
        mapper = Mappers.getMapper(BusinessEntityMapper.class);
    }

    @Test
    void toEntity() {
        var model = new Business(
                UUID.randomUUID(),
                new BusinessTitle("title"),
                new BusinessElementNumber(1),
                new Category(1)
        );

        var actual = mapper.toEntity(model);

        assertEquals(model.getId(), actual.getId());
        assertEquals(model.getCategory(), actual.getCategoryId());
        assertEquals(model.getTitle().getTitle(), actual.getTitle());
        assertEquals(model.getElements().get(0).getNumber(), actual.getElements().get(0).getNumber());
    }

    @Test
    void toModel() {
        var entity = new BusinessEntity();
        entity.setId(UUID.randomUUID());
        entity.setCategoryId(1);
        entity.setTitle("Title");

        var actual = mapper.toModel(entity);

        assertEquals(entity.getId(), actual.getId());
        assertEquals(entity.getCategoryId(), actual.getCategory());
        assertEquals(entity.getTitle(), actual.getTitle().getTitle());
    }

    @Test
    void toEntities() {
        var model = new Business(
                UUID.randomUUID(),
                new BusinessTitle("title"),
                new BusinessElementNumber(1),
                new Category(1)
        );
        var actual = mapper.toEntities(List.of(model));

        assertEquals(1, actual.size());
    }

    @Test
    void toModels() {
        var entity = new BusinessEntity();
        entity.setId(UUID.randomUUID());
        entity.setCategoryId(1);
        entity.setTitle("Title");

        var actual = mapper.toModels(List.of(entity));

        assertEquals(1, actual.size());
    }
}