package com.poc.service.infrastructure.service.persistence.repository;

import com.poc.service.domain.BusinessRepository;
import com.poc.service.domain.core.Category;
import com.poc.service.domain.core.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BusinessRepositoryImplTest {
    @Autowired
    private BusinessRepository businessRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    @Transactional
    void findAll() {
        Category category = new Category(1);
        categoryRepository.save(category);

        category.doBusiness("title", 1);
        categoryRepository.save(category);

        var actual = businessRepository.findAll();

        assertEquals(1, actual.size());
    }
}