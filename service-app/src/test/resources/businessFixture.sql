INSERT INTO category VALUES (1);

-- business
INSERT INTO business (id, title, category_id, created_at) VALUES
('2D1EBC5B7D2741979CF0E84451C5BBB1', 'title', 1, '2021-06-06 17:35:57');

-- element
INSERT INTO element (id, business_id, number, created_at, updated_at)
VALUES (
   RANDOM_UUID(),
   '2D1EBC5B7D2741979CF0E84451C5BBB1',
   1,
   '2021-05-06 17:35:57',
   '2021-05-06 17:35:57'
);
