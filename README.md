# Service
An example of DDD-oriented microservice.

* Service - Spring Boot microservice divided into levels:
  * Application - API, controllers, application services.
  * Domain - framework-agnostic business logic.
  * Infrastructure - DB adapters, cache, emails, logs, etc.
* Client - OpenFeign client jar for communicating between microservices.
* API - contracts and DTOs between service and the Client.

[Mapstruct](https://mapstruct.org/) is a mapper for DTOs, model objects and entities.

![Event Storming Graphics](assets/event_storming.png "event storming graphics")

Business is root for its elements to prevent duplicates and force to create a Business with at
least one Element.
On the other hand Category is an entity of another (core) domain, but it's an aggregation root (invariants keeper) for
service's Business because Business cannot be created without a Category. It means that Business Element as a Business
can be saved only via Category repository.

## Async Events
* Category created.
* Category deleted.

# Gradle Dependency
* Client - `implementation 'com.poc.service:service-client:<version>`

# Local development
To download service dependencies update *gradle.properties* file in project root. Set you gitlab token.

Run `docker-compose up`.

To run the app with environment variabled add the `EnvFile` plugin to IDE.

RabbitMQ admin panel: `http://localhost:15672/`, `rmquser:rmqpassword`. Payload:
```json
{
    "messageId": "02bce6d1-fb08-48d0-8ede-9cbae551fa73",
    "timestamp": "1610893796",
    "payload":  "3"
}
```
## Generate docker image
* Run `jibDockerBuild` gradle task with `CI_COMMIT_SHORT_SHA` env variable for as tag.

## Postman
![Collection](assets/poc-service.postman_collection.json "event storming graphics")

## Actuator
* `/actuator/health/liveness`
* `/actuator/health/readiness`
* `/actuator/mappings`
  * REST endpoints: `curl http://localhost:8090/actuator/mappings | jq '.contexts.service.mappings.dispatcherServlets
 .dispatcherServlet[] | select(.handler | contains("com.poc.service"))'`

# Deployment
* Increment version.
* Run pipeline on `master` branch.

# SonarQube
* Go to https://sonarcloud.io/organizations/poc48/projects

# Checkstyle
* Install [IDE plugin](https://plugins.jetbrains.com/plugin/1065-checkstyle-idea).
* Add a custom styles by selecting the `config/checkstyle/checkstyle.xml`.

#TODO
* Add business element endpoint.
* Admin data on infrastructure level (form over data).
* Cover with lombok @NonNull
* A separate Auth service and here add dependency to spring security via common. 
* Check all error labels.
* Spring Security to common module to share between service and gateway.
* Explanations of services and interaction.
* ACL on service level
* Deployment docs.
